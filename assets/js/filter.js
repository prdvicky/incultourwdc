$(document).ready()

  function showDiscoverImage(){
    var userInput = $("#filter").val();
    if (userInput == 'all'){
      $(".price, .near, .popular").css({"display":"block"});

    } else if (userInput == 'popular'){
      $('.popular').css({"display":"block"});
      $('.near, .price').css({"display":"none"});

    } else if (userInput == 'nearest'){
      $('.near').css({"display":"block"});
      $('.popular, .price').css({"display":"none"});

    } else {
      $('.price').css({"display":"block"});
      $('.near, .popular').css({"display":"none"});
    }
  }
