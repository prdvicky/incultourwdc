$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});


if($(window).width() >= 768){
 	$(window).scroll(function() {
		if ($(this).scrollTop()>50) {
			$(".slideshow-caption").addClass('opacity-0');
			// NAVBAR 1
			$(".nav").css({
				"background-color":"white",
				"box-shadow":"0 0 5px rgba(0,0,0,.2)"
			})
			$(".menu a").css({
				"padding":"15px",
				"color":"black",
			})
			$('#nav .menu a i').css({"color":"black"})
			$(".menu a.login-btn").css({
				"padding":"10px 30px",
				"color":"white",
				"background-color":"#02B8B9",
				"border-bottom":"1px solid #02B8B9"
			})
			$(".menu a.act").css({"border-bottom":"1px solid black"})
			$(".fil0").css({"fill":"none"})
			$(".fil1").css({"fill":"black"})
			$(".menu li").css({"padding":"25px 10px"})
			$(".mobile-nav svg").css({
				"height":"70px",
				"padding":"10px 70px"
			})
			$('#nav .menu a:not(.act)').hover(
				function(){
					$(this).css({"border-bottom":"1px solid black"})
				},
				function(){
					$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
			   	}
			)
			$('#nav .menu a.login-btn').hover(
				function(){
					$(this).css({"border-bottom":"1px solid #02B8B9"})
				},
				function(){
					$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
			   	}
			)


			// NAVBAR-2
			$(".slideshow-caption").addClass('opacity-0');
			$("#nav-2 .nav").css({
				"background-color":"white",
				"box-shadow":"0 0 5px rgba(0,0,0,.2)"
			})
			$("#nav-2 .menu a").css({
				"padding":"15px",
				"color":"black",
			})
			$('#nav-2 .menu a i').css({"color":"black"})
			$("#nav-2 .menu a.login-btn").css({
				"padding":"10px 30px",
				"color":"white",
				"background-color":"#02B8B9",
				"border-bottom":"1px solid #02B8B9"
			})
			$("#nav-2 .menu a.act").css({"border-bottom":"1px solid black"})
			$("#nav-2 .fil0").css({"fill":"none"})
			$("#nav-2 .fil1").css({"fill":"black"})
			$("#nav-2 .menu li").css({"padding":"25px 10px"})
			$("#nav-2 .mobile-nav svg").css({
				"height":"70px",
				"padding":"10px 70px"
			})
			$('#nav-2 .menu a:not(.act)').hover(
				function(){
				    $(this).css({"border-bottom":"1px solid black"})
				    },
				    function(){
				    $(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
				}
			)
			$('#nav-2 .menu a.login-btn').hover(
				function(){
				    $(this).css({"border-bottom":"1px solid #02B8B9"})
				    },
				    function(){
				    $(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
				}
			)


			// ANOTHER SETTINGS SCROLLED
			$("#nav-dropdown").css({"margin-top":"100px"})
			$("#dropdown").hover(
				function(){
				    $('#nav, #nav-2').css({"background-color": "white"});
				    $('#nav .menu-dropdown li, #nav-2 .menu-dropdown li').css({
				    	"transform": "translate3d(0px, 0px, 0px)"
				    });
				    $('#nav .menu a').css({
				    	"color": "black"
				    });
				    $('#nav .menu a i, #nav-2 .menu a i').css({"color":"#02B8B9"})
				    $('#nav .menu a.act').css({
				    	"border-bottom": "1px solid black"
				    });
				    $('#nav .menu a.login-btn').css({
				    	"color": "white",
				    	"background-color":"#02B8B9",
				    	"border-bottom":"1px solid #02B8B9"
				    });
				    $("#nav .fil1").css({"fill":"black"})
				    $('#nav .menu-dropdown, #nav-2 .menu-dropdown').css({
				    	// "display": "block"
				    	"margin-top": "25px",
				    	"opacity":"1",
				    	"visibility":"visible"
				    })
				    $('#nav .menu a:not(.act)').hover(
						function(){
							$(this).css({"border-bottom":"1px solid black"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
					   	}
					)
					$('#nav .menu a.login-btn').hover(
						function(){
							$(this).css({"border-bottom":"1px solid #02B8B9"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid #02B8B9"})
					   	}
					)
					$('.black-transparent').css({
						"opacity":"0.7",
						"visibility":"visible"
					})
				    
			    },
			    function(){
				    $('#nav, #nav-2').css({"background-color": "white"});
				    $('#nav .menu-dropdown li, #nav-2 .menu-dropdown li').css({
				    	"transform": "translate3d(0px, -500px, 0px)"
					});
					$('#nav .menu a').css({
				    	"color": "black"
				    });
				    $('#nav .menu a i, #nav-2 .menu a i').css({"color":"black"})
				    $('#nav .menu a.act').css({
				    	"border-bottom": "1px solid black"
				    });
				    $('#nav .menu a.login-btn').css({
				    	"color": "white",
				    	"background-color":"#02B8B9",
				    	"border-bottom":"1px solid #02B8B9"
				    });
				    $("#nav .fil1").css({"fill":"black"})
				    $('#nav .menu-dropdown, #nav-2 .menu-dropdown').css({
				    	// "display": "none"
				    	"margin-top": "25px",
				    	"opacity":"1",
				    	"visibility":"hidden"
				    })
				    $('#nav .menu a:not(.act)').hover(
						function(){
							$(this).css({"border-bottom":"1px solid black"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
					   	}
					)
					$('#nav .menu a.login-btn').hover(
						function(){
							$(this).css({"border-bottom":"1px solid #02B8B9"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid #02B8B9"})
					   	}
					)
					$('.black-transparent').css({
						"opacity":"0",
						"visibility":"hidden"
					})
			});
			
		}
		else {
			$(".slideshow-caption").removeClass('opacity-0');
			// NAVBAR 1
			$(".nav").css({
				"background-color":"",
				"box-shadow":"none"
			})
			$(".menu a").css({
				"padding":"15px",
				"color":"white"
			})
			$('#nav .menu a i').css({"color":"white"})
			$(".menu a.login-btn").css({
				"padding":"10px 30px",
				"color":"black",
				"background-color":"#f2f2f2",
				"border-bottom":"2px solid #f2f2f2"
			})
			$(".menu a.act").css({"border-bottom":"2px solid white"})
			$(".fil0").css({"fill":"none"})
			$(".fil1").css({"fill":"white"})
			$(".menu li").css({"padding":"40px 10px"})
			$(".mobile-nav svg").css({
				"height":"100px",
				"padding":"20px 70px"
			})
			$('#nav .menu a:not(.act)').hover(
				function(){
					$(this).css({"border-bottom":"2px solid white"})
				},
				function(){
					$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
			   	}
			)
			$('#nav-2 .menu a.login-btn').hover(
				function(){
				    $(this).css({"border-bottom":"2px solid #f2f2f2"})
				    },
				    function(){
				    $(this).css({"border-bottom":"2px solid rgba(0,0,0,0)"})
				}
			)

			// NAVBAR-2
			$("#nav-2 .nav").css({
				"background-color":"",
				"box-shadow":"none"
			})
			$("#nav-2 .menu a").css({
				"padding":"15px",
				"color":"black"
			})
			$('#nav-2 .menu a i').css({"color":"black"})
			$("#nav-2 .menu a.login-btn").css({
				"padding":"10px 30px",
				"color":"black",
				"background-color":"#f2f2f2",
				"border-bottom":"2px solid #f2f2f2"
			})
			$("#nav-2 .menu a.act").css({"border-bottom":"2px solid black"})
			$("#nav-2 .fil0").css({"fill":"none"})
			$("#nav-2 .fil1").css({"fill":"black"})
			$("#nav-2 .menu li").css({"padding":"40px 10px"})
			$("#nav-2 .mobile-nav svg").css({
				"height":"100px",
				"padding":"20px 70px"
			})
			$("#nav-2 .mobile-nav").css({
				"box-shadow":"none"
			})
			$('#nav-2 .menu a:not(.act)').hover(
				function(){
				    $(this).css({"border-bottom":"2px solid black"})
				    },
				    function(){
				    $(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
				}
			)
			$('#nav-2 .menu a.login-btn').hover(
				function(){
				    $(this).css({"border-bottom":"2px solid #f2f2f2"})
				    },
				    function(){
				    $(this).css({"border-bottom":"2px solid rgba(0,0,0,0)"})
				}
			)

			// ANOTHER SETTINGS
			$("#nav-dropdown").css({"margin-top":"100px"})
			$("#dropdown").hover(
				function(){

				    $('#nav, #nav-2').css({"background-color": "white"});
				    $('#nav .menu-dropdown li, #nav-2 .menu-dropdown li').css({
				    	"transform": "translate3d(0px, 0px, 0px)"
				    });
				    $('#nav .menu a').css({
				    	"color": "black"
				    });
				    $('#nav .menu a i, #nav-2 .menu a i').css({"color":"#02B8B9"})
				    $('#nav .menu a.act').css({
				    	"border-bottom": "2px solid black"
				    });
				    $("#nav .menu a.login-btn").css({
						"padding":"10px 30px",
						"color":"white",
						"background-color":"#02B8B9",
						"border-bottom":"2px solid #02B8B9"
					})
				    $("#nav .fil1").css({"fill":"black"})
				    $('#nav .menu-dropdown a, #nav-2 .menu-dropdown a').css({
				    	// "display": "block"
				    	"font-family":"op-extrabold",
				    	"font-size":"2.5em",
				    	"color":"white!important",
				    	"z-index":"20",
				    	"height":"70%!important",
				    	"border":"none!important"
				    })
				    $('#nav .menu-dropdown, #nav-2 .menu-dropdown').css({
				    	// "display": "block"
				    	"margin-top": "40px",
				    	"opacity":"1",
				    	"visibility":"visible"
				    })
				    $('#nav .menu a:not(.act)').hover(
						function(){
							$(this).css({"border-bottom":"2px solid black"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
					   	}
					)
					$('#nav .menu a:not(#dropped-down)').hover(
						function(){
							$(this).css({"border-bottom":"2px solid white"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
					   	}
					)
					$('.black-transparent').css({
						"opacity":"0.7",
						"visibility":"visible"
					})
			    },
			    function(){
				    $('#nav, #nav-2').css({"background-color": ""});
				    $('#nav .menu-dropdown li, #nav-2 .menu-dropdown li').css({
				    	"transform": "translate3d(0px, -500px, 0px)"
					});
					$('#nav .menu a').css({
				    	"color": "white"
				    });
				    $('#nav .menu a i').css({"color":"white"})
				    $('#nav-2 .menu a i').css({"color":"black"})
				    $('#nav .menu a.act').css({
				    	"border-bottom": "2px solid white"
				    });
				    $("#nav .menu a.login-btn").css({
						"padding":"10px 30px",
						"color":"white",
						"background-color":"#f2f2f2",
						"border-bottom":"2px solid #f2f2f2"
					})
				    $('#nav .menu a.login-btn').css({
				    	"color": "black"
				    });
				    $("#nav .fil1").css({"fill":"white"})
				    $('#nav .menu-dropdown, #nav-2 .menu-dropdown').css({
				    	// "display": "none"
				    	"margin-top": "40px",
				    	"opacity":"1",
				    	"visibility":"hidden"
				    })
				    $('#nav .menu a:not(.act)').hover(
						function(){
							$(this).css({"border-bottom":"2px solid white"})
						},
						function(){
							$(this).css({"border-bottom":"1px solid rgba(0,0,0,0)"})
					   	}
					)
					$('.black-transparent').css({
						"opacity":"0",
						"visibility":"hidden"
					})
			});
		}
	});
}
else {
	$(".fil1").css({"fill":"black"})
	$(".nav").css({
		"padding":"0",
	})
	$(".menu a").css({
		"font-size":"1em",
	})
	$(".menu a").css({
		"padding":"15px",
		"color":"black"
	})
	$(".menu a.act").css({"border-bottom":"2px solid black"})
	$(".menu a.login-btn").css({
		"padding":"10px 30px",
		"color":"white"
	})
	$('#nav .menu-dropdown').css({
		"visibility":"hidden",
		"opacity":"0"
	})
	// $('#nav .mobile-nav svg').css({
	// 	"width":"40px";
	// })

	// NAVBAR-2
	$("#nav-2 .fil1").css({"fill":"black"})
	$("#nav-2 .nav").css({
		"padding":"0",
	})
	$("#nav-2 .menu a").css({
		"font-size":"1em",
	})
	$("#nav-2 .menu a").css({
		"padding":"15px",
		"color":"black"
	})
	$("#nav-2 .mobile-nav").css({
		"box-shadow":"0 0 5px rgba(0,0,0,.2)"
	})
	$("#nav-2 .menu a.act").css({"border-bottom":"2px solid black"})
	$("#nav-2 .menu a.login-btn").css({
		"padding":"10px 30px",
		"color":"white"
	})
}