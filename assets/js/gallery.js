$(document).ready(function(){
    // ANGKLUNG
    $('#popup-ang').click(function(){
      $('.bg-ang').addClass('bg-transparent-show');
      $('.angklung').addClass('popup-container-show');
      $('.close-ang').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-ang').removeClass('bg-transparent-show');
      $('.angklung').removeClass('popup-container-show');
      $('.close-ang').removeClass('close-popup-btn-show');
      $('#vid').each(function(){ this.pause() });
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-ang').removeClass('bg-transparent-show');
    //   $('.angklung').removeClass('popup-container-show');
    //   $('.close-ang').removeClass('close-popup-btn-show');
    // })

    // BARONG
    $('#popup-bar').click(function(){
      $('.bg-bar').addClass('bg-transparent-show');
      $('.barong').addClass('popup-container-show');
      $('.close-bar').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-bar').removeClass('bg-transparent-show');
      $('.barong').removeClass('popup-container-show');
      $('.close-bar').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-bar').removeClass('bg-transparent-show');
    //   $('.barong').removeClass('popup-container-show');
    //   $('.close-bar').removeClass('close-popup-btn-show');
    // })
    // BATIK
    $('#popup-bat').click(function(){
      $('.bg-bat').addClass('bg-transparent-show');
      $('.batik').addClass('popup-container-show');
      $('.close-bat').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-bat').removeClass('bg-transparent-show');
      $('.batik').removeClass('popup-container-show');
      $('.close-bat').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-bat').removeClass('bg-transparent-show');
    //   $('.batik').removeClass('popup-container-show');
    //   $('.close-bat').removeClass('close-popup-btn-show');
    // })
    // KARAPAN
    $('#popup-kar').click(function(){
      $('.bg-kar').addClass('bg-transparent-show');
      $('.karapan').addClass('popup-container-show');
      $('.close-kar').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-kar').removeClass('bg-transparent-show');
      $('.karapan').removeClass('popup-container-show');
      $('.close-kar').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-kar').removeClass('bg-transparent-show');
    //   $('.karapan').removeClass('popup-container-show');
    //   $('.close-kar').removeClass('close-popup-btn-show');
    // })
    // KASADA
    $('#popup-kas').click(function(){
      $('.bg-kas').addClass('bg-transparent-show');
      $('.kasada').addClass('popup-container-show');
      $('.close-kas').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-kas').removeClass('bg-transparent-show');
      $('.kasada').removeClass('popup-container-show');
      $('.close-kas').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-kas').removeClass('bg-transparent-show');
    //   $('.kasada').removeClass('popup-container-show');
    //   $('.close-kas').removeClass('close-popup-btn-show');
    // })
    // KECAK
    $('#popup-kec').click(function(){
      $('.bg-kec').addClass('bg-transparent-show');
      $('.kecak').addClass('popup-container-show');
      $('.close-kec').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-kec').removeClass('bg-transparent-show');
      $('.kecak').removeClass('popup-container-show');
      $('.close-kec').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-kec').removeClass('bg-transparent-show');
    //   $('.kecak').removeClass('popup-container-show');
    //   $('.close-kec').removeClass('close-popup-btn-show');
    // })
    // REOG
    $('#popup-reo').click(function(){
      $('.bg-reo').addClass('bg-transparent-show');
      $('.reog').addClass('popup-container-show');
      $('.close-reo').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-reo').removeClass('bg-transparent-show');
      $('.reog').removeClass('popup-container-show');
      $('.close-reo').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-reo').removeClass('bg-transparent-show');
    //   $('.reog').removeClass('popup-container-show');
    //   $('.close-reo').removeClass('close-popup-btn-show');
    // })
    // SAMAN
    $('#popup-sam').click(function(){
      $('.bg-sam').addClass('bg-transparent-show');
      $('.saman').addClass('popup-container-show');
      $('.close-sam').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-sam').removeClass('bg-transparent-show');
      $('.saman').removeClass('popup-container-show');
      $('.close-sam').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-sam').removeClass('bg-transparent-show');
    //   $('.saman').removeClass('popup-container-show');
    //   $('.close-sam').removeClass('close-popup-btn-show');
    // })
    // WAYANG
    $('#popup-way').click(function(){
      $('.bg-way').addClass('bg-transparent-show');
      $('.wayang').addClass('popup-container-show');
      $('.close-way').addClass('close-popup-btn-show');
    })
    $('.close-popup-btn').click(function(){
      $('.bg-way').removeClass('bg-transparent-show');
      $('.wayang').removeClass('popup-container-show');
      $('.close-way').removeClass('close-popup-btn-show');
    })
    // $('.bg-transparent').click(function(){
    //   $('.bg-way').removeClass('bg-transparent-show');
    //   $('.wayang').removeClass('popup-container-show');
    //   $('.close-way').removeClass('close-popup-btn-show');
    // })
})